var input = document.querySelectorAll("input");


    for(var i = 0; i < input.length; i++){
        input[i].addEventListener("input", function(){
            var red = document.getElementById("red").value,
                green = document.getElementById("green").value,
                blue = document.getElementById("blue").value;

            var display = document.getElementById("display");
            display.style.background = "rgb(" + red + ", " + green + ", " + blue +")";                    
        });                     
    }


    document.getElementById("changecolors").addEventListener("click", changeBackground);

    function changeBackground(){
        
        var red = document.getElementById("red").value,
            green = document.getElementById("green").value,
            blue = document.getElementById("blue").value;

            var display = document.getElementById("display");
            display.style.background = "rgb(" + red + ", " + green + ", " + blue +")";

    document.body.style.backgroundColor = display.style.background;           
    }   

    document.getElementById("reset").addEventListener("click", resetSettings);
    function resetSettings(){
        document.body.style.backgroundColor = "white";
    }

    document.getElementById("randomcolors").addEventListener("click", changeColorRandomly);

    function changeColorRandomly(){
        if(confirm("Are you sure you want to continiue?") == true) { 
            if(confirm("You will NOT be able to stop it!") == true){ 
                if(confirm("Ok... press OK then...") == true) {          

                    setInterval(function(){ 
                    var red = Math.floor(Math.random()*255),
                    green = Math.floor(Math.random()*255),
                    blue = Math.floor(Math.random()*255);
                    
                    document.body.style.backgroundColor = "rgb(" + red + ", " + green + ", " + blue +")";

                }, 2000);
            }
        }
    } 
}
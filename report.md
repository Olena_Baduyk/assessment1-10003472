# ASSIGNMENT 1 / Report
 
**Student Name:** Olena Spektor

**Student ID:** 10003472

Here, I'll discribe the practicies I think should be put on first priority in GUI.

## 1. Do a research
It would be a good rule if all the designers were involved in psychology and marketing industry. It would give them an opportunity to get some skills how to do a proper research and how to analyse it in the end. However, it would be enough for beginners in design at least to consider such things as who is the end user (e.g, demographically, gender, age, culture), how the end user is going to use an application (e.g, touch screen, mouse clicks), would it be possible to test the design of an application before it releases. 

If the designer knows the end user well or at least performed a bit research, it would be easier for him to create the most comfortable in use product. 

Reference: [Best practices for designing a graphical user interface](http://beijerinc.com/pdf/whitepaper/interface_design_best_practices.pdf)

## 2. Portability
Because of the variety of programming languages and platforms, not all of the already created applications can be used on any OS. Not so long ago, developers needed to create a separate version of an app for Windows, Android, Linux, MAC and others OS. Luckily nowadays this problem is solved and there are some libraries that allow developers to create just one version of an app and run it on multiple devices with different OS at the same time. 

One of such libraries is Electron, allows to create desktop applications on the one operating system and easily run it on the other one without installing lots of extra features on it. The process is done by taking the main file from a package.json file (which is a JavaScript file and usually named as main.js) and integrating it with the native GUI of the computer OS. After that, the app opens in the browser window, which has got the same design as the native operating system.

Currently, Electron runs on Windows, Linux, and Mac operating systems. 

Reference: [Building a desktop application with Electron](https://medium.com/developers-writing/building-a-desktop-application-with-electron-204203eeb658).

## 3. Layout
Like in a web design, the design of a web application also must be simple and comfortable to use. It must be clear to the end users what buttons to push and what their next destination in the app. 

There are a couple of rules and technics that help to achieve this:
- Call to action (there should be many "calls to actions" on a page, otherwise it may confuse a user and he leaves the app). Ideally, call to action must be one per page and it should be specified clearly. Example - Uber app.
-  Add white space to the design to make it breathable. It helps to focus users attention on one thing and nothing else and makes it faster to understand how to act on the app for new users. Example - Grammarly.

Reference: [Mobile Design Best Practices](https://uxplanet.org/mobile-design-best-practices-2d16d37ecfe)

## 4.  Navigation 
Navigation must always be accessible on any app page. In case a user wants to change some information that he previously set up he always must be able to go back and do the changes.

The navigation items also must clear and simple. Google material design makes it the best:
- Usage of icons instead of naming the buttons to show a user what to do. 
- Usage of one button to hide and pop up the options of actions in an app. For example, this picture ![](https://www.smashingmagazine.com/wp-content/uploads/2016/11/floating-action-button-material-design.png)

Reference: [The Golden Rules Of Bottom Navigation Design](https://www.smashingmagazine.com/2016/11/the-golden-rules-of-mobile-navigation-design/)


## 5. Interaction with user
Interaction design is important as it helps to understand users what is going on on the app and how long a chosen action proceeds. That is why these rules must be followed:
- Label/buttons names, headers and descriptions must be short and simple.
- The shape of the objects and space around it must be created after considering such things as how a user is going to use it (using mouse or touchscreen), what environment and day time is it going to be (a good example of it a reading app, like Kindle, where the screen can become brighter or lighter depending on the day time) and many others that can help to make an app more comfortable.


Reference: [What is Interaction Design?](https://www.interaction-design.org/literature/article/what-is-interaction-design)


## 6. Always put Black & White first
It's not about creating wireframes. And it's not about using only white and black colours to a design to apply Monochrome style to it. It is about the technic of creating an easy looking design without "parrot" styling. 

The principle is that, based on your wireframes, you need to create the real design of the app in two colours first - black and white only. Once it's done, only then you start to change the colours of the objects of your design. 

This technic helps to create extra space in your design that makes it easier to read and follow and also makes the app look "clean" and "simple". It's good to know the psychological tricks with colours, like what colour attracts the most attention and others, but it would be better practice also to know the way how much of those colours to use to not make the user to leave the application. Black & White First is one of the principles that help to achieve it.

Reference: [7 Rules for Creating Gorgeous UI](https://medium.com/@erikdkennedy/7-rules-for-creating-gorgeous-ui-part-1-559d4e805cda)


## 7. Fonts 
Fonts aren't less important in the design. Together with other design features properly set up fonts are helping to read and scan the content faster. 

There some tips help to achieve this goal:
- Always add a serif or monospace to the CSS to set up back up a font in case the user's browser doesn't support others.
- Don't make the font too small.
- Don't use all upper case letters, it makes it difficult to read.

References:

[Best practices for designing a graphical user interface](http://beijerinc.com/pdf/whitepaper/interface_design_best_practices.pdf)

[Choosing web fonts: 15 expert tips](http://www.creativebloq.com/web-design/choose-web-fonts-1233034)


## 8. A / B testing for web applications
There many business web applications where people always buy or sell something on the internet. There many marketing strategies to increase the productivity of an app, like a survey and/or focus-group. And there is another one, a/b testing, that involves all the target audience and helps to save money on research. 

This kind of research, for example, can help to identify what shape and colour button make people to click on a button and finish their purchase, or what background texture makes users to stay longer in an app and so on.

Reference: [A/B Testing](https://www.optimizely.com/ab-testing/)